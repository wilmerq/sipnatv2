package co.edu.ucc.sipnat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ucc.sipnat.dto.NotificationDTO;
import co.edu.ucc.sipnat.service.NotificationService;

@Controller
@RequestMapping(value = "/notification")
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@GetMapping
	public ResponseEntity<List<NotificationDTO>> list() {
		return new ResponseEntity<>(notificationService.getList(), HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<NotificationDTO> getById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(notificationService.get(id), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<?> updateNotification(@RequestBody NotificationDTO notificationDTO) {
		notificationService.updateNotification(notificationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(path = "/count")
	public ResponseEntity<Long> countNotificationsActive() {
		return new ResponseEntity<>(notificationService.countActiveNotification(), HttpStatus.OK);
	}
}
