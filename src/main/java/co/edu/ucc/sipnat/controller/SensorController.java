package co.edu.ucc.sipnat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ucc.sipnat.dto.SensorDTO;
import co.edu.ucc.sipnat.dto.SensorDetailDTO;
import co.edu.ucc.sipnat.service.SensorService;

@Controller
@RequestMapping(value = "/sensor")
public class SensorController {

	@Autowired
	private SensorService sensorService;

	@PostMapping
	public ResponseEntity<Object> createSensor(@RequestBody SensorDTO sensorDTO) {
		return new ResponseEntity<>(sensorService.create(sensorDTO), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<List<SensorDetailDTO>> list() {
		return new ResponseEntity<>(sensorService.listSensors(), HttpStatus.OK);
	}

}
