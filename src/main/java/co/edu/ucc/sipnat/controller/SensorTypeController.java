package co.edu.ucc.sipnat.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import co.edu.ucc.sipnat.dto.SensorTypeDTO;
import co.edu.ucc.sipnat.service.SensorTypeService;

@RestController
@RequestMapping(value = "/sensortype")
public class SensorTypeController {

	@Autowired
	private SensorTypeService sensorTypeService;

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, headers = ("content-type=multipart/*"))
	public ResponseEntity<Boolean> create(
			@RequestParam("name") String name,
			@RequestParam("magnitude") String magnitude,
			@RequestParam("unitMeasure") String unitMeasure,
			@RequestParam("icon") MultipartFile icon) throws Exception {
	

		SensorTypeDTO sensorTypeDTO = new SensorTypeDTO();
		sensorTypeDTO.setName(name);
		sensorTypeDTO.setMagnitude(magnitude);
		sensorTypeDTO.setUnitMeasure(unitMeasure);
		sensorTypeDTO.setFile(icon);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.buildAndExpand(sensorTypeService.createSensorType(sensorTypeDTO)).toUri();

		return ResponseEntity.created(location).build();
	}

	@GetMapping	
	public ResponseEntity<List<SensorTypeDTO>> list() {
		return new ResponseEntity<>(sensorTypeService.getList(), HttpStatus.OK);
	}
	
	@GetMapping(path = "/icon/{id}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
	public byte[] getIcon(@PathVariable("id") Long id) {
		return sensorTypeService.getIcon(id);
	}

}
