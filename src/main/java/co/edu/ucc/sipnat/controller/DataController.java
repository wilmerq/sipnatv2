package co.edu.ucc.sipnat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ucc.sipnat.dto.response.DataSensorResponseDTO;
import co.edu.ucc.sipnat.service.DatoService;

@Controller
@RequestMapping(value = "/data")
public class DataController {

	@Autowired
	private DatoService datoService;

	@GetMapping(path = "/{idSensor}")
	public ResponseEntity<DataSensorResponseDTO> getTodayDataBySensor(@PathVariable("idSensor") Long idSensor) {
		return new ResponseEntity<>(datoService.getDataSensorToday(idSensor), HttpStatus.OK);
	}

	@GetMapping(path = "/{idSensor}/all")
	public ResponseEntity<DataSensorResponseDTO> getAllDataBySensor(@PathVariable("idSensor") Long idSensor) {
		return new ResponseEntity<>(datoService.getAllDataSensor(idSensor), HttpStatus.OK);
	}
}
