package co.edu.ucc.sipnat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ucc.sipnat.dto.DashboardDTO;
import co.edu.ucc.sipnat.dto.SensorDetailDTO;
import co.edu.ucc.sipnat.dto.response.DataSensorResponseDTO;
import co.edu.ucc.sipnat.service.DashboardService;

@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@PostMapping
	public ResponseEntity<Object> createDashboard(@RequestBody DashboardDTO dto) {
		return new ResponseEntity<>(dashboardService.createDashboard(dto), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<List<DashboardDTO>> getAllDashboard() {
		return new ResponseEntity<>(dashboardService.getAllDashboard(), HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<DashboardDTO> getDashboard(@PathVariable("id") Long idDashboard) {
		return new ResponseEntity<>(dashboardService.getDataDashboard(idDashboard), HttpStatus.ACCEPTED);
	}

	@GetMapping(path = "/{id}/sensors/")
	public ResponseEntity<List<SensorDetailDTO>> getSensorsDashboard(@PathVariable("id") Long idDashboard) {
		return new ResponseEntity<>(dashboardService.getSensorsDashboard(idDashboard), HttpStatus.OK);
	}

	@GetMapping(path = "/{id}/sensors/data/{type}")
	public ResponseEntity<List<DataSensorResponseDTO>> getDataSensorsDashboard(@PathVariable("id") Long idDashboard,
			@PathVariable("type") String type) {
		return new ResponseEntity<>(dashboardService.getDataSensorsDashboard(idDashboard, type), HttpStatus.ACCEPTED);
	}

	@GetMapping(path = "/{id}/sensors/state/{state}")
	public ResponseEntity<Long> getStateSensorsDashboard(@PathVariable("id") Long idDashboard,
			@PathVariable("state") String state) {
		return new ResponseEntity<>(dashboardService.countSensorsByState(idDashboard, state), HttpStatus.OK);
	}

}
