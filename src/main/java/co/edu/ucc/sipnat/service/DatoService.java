package co.edu.ucc.sipnat.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IDatoDAO;
import co.edu.ucc.sipnat.dto.response.DataSensorResponseDTO;
import co.edu.ucc.sipnat.entity.AclMosquitto;
import co.edu.ucc.sipnat.entity.Dato;
import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.exception.ExceptionLogicBusiness;
import co.edu.ucc.sipnat.mapper.DataMapper;
import co.edu.ucc.sipnat.util.Utils;

@Service
public class DatoService {
	
	private Logger logger = LogManager.getLogger(DatoService.class);

	@Autowired
	private ACLMosquittoService aclMosquittoService;
	@Autowired
	private IDatoDAO datoDAO;
	@Autowired
	private SensorService sensorService;
	@Autowired
	private DataMapper dataMapper;

	@Async
	public void receiveData(String topic, String message, String protocol) {
		try {
			logger.info("receiveData {}|{}|{}", topic, message, protocol);
			if (message != null && !message.trim().isEmpty()) {
				create(topic, message, protocol);
			}
		} catch (Exception e) {
			logger.error("receiveData", e);
		}
	}

	public DataSensorResponseDTO getDataSensorToday(Long idSensor) {
		Sensor sensor = sensorService.getById(idSensor);

		if (sensor == null) {
			throw new ExceptionLogicBusiness("Sensor not exist", HttpStatus.BAD_REQUEST);
		}

		return dataMapper.entityToDto(sensor,
				datoDAO.getDataSensorByFechaRecoleccion(idSensor, Utils.getCurrentDate()));
	}

	public DataSensorResponseDTO getAllDataSensor(Long idSensor) {
		Sensor sensor = sensorService.getById(idSensor);

		if (sensor == null) {
			throw new ExceptionLogicBusiness("Sensor not exist", HttpStatus.BAD_REQUEST);
		}

		return dataMapper.entityToDto(sensor, datoDAO.getAllDataSensor(idSensor));
	}

	public List<Dato> getAllDataNotRevised(Sensor sensor) {
		return datoDAO.getAllDataSensorNotRevised(sensor.getId());
	}

	public Date dateLastData(Sensor sensor) {
		Dato dato = datoDAO.getFirstDatoByFechaRecoleccion(sensor.getId());
		return dato == null ? null : dato.getFechaRecoleccion();
	}

	private Sensor getSensorByTopic(String topic) throws Exception {
		List<AclMosquitto> listACLMosquitto = aclMosquittoService.listACLMosquitto();
		List<AclMosquitto> listACLMosquittoFiltered = listACLMosquitto.stream()
				.filter(acl -> acl.getTopic().equals(topic)).collect(Collectors.toList());

		if (listACLMosquittoFiltered == null || listACLMosquittoFiltered.isEmpty()) {
			throw new Exception("Topic don't exist");
		}

		if (listACLMosquittoFiltered.size() > 1) {
			throw new Exception("Topic duplicate  in " + listACLMosquittoFiltered.size());
		}

		return listACLMosquittoFiltered.get(0).getSensor();
	}

	public Dato create(String topic, String message, String protocol) throws Exception {
		Dato dato = new Dato();

		dato.setFechaRecoleccion(new Date());
		dato.setSensor(getSensorByTopic(topic));
		dato.setRevisado(false);
		dato.setValor(message);
		dato.setProtocolo(protocol);

		return datoDAO.save(dato);
	}

	public void updateDato(Dato dato) {
		datoDAO.save(dato);
	}

}
