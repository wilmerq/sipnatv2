package co.edu.ucc.sipnat.mapper;

import java.util.Date;

import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.ACLMosquittoDTO;
import co.edu.ucc.sipnat.dto.SensorDTO;
import co.edu.ucc.sipnat.entity.AclMosquitto;
import co.edu.ucc.sipnat.entity.UserMosquitto;
import co.edu.ucc.sipnat.util.Utils;

@Service
public class ACLMosquittoMapper {

	public AclMosquitto dtoToEntity(SensorDTO dto, UserMosquitto userMosquitto, String magnitude) {
		AclMosquitto aclMosquitto = new AclMosquitto();

		aclMosquitto.setTopic(Utils.buildTopic(dto.getName(), magnitude));
		aclMosquitto.setSuscriptionPermission(Boolean.TRUE);
		aclMosquitto.setWritePermission(Boolean.TRUE);
		aclMosquitto.setReadPermission(Boolean.FALSE);
		aclMosquitto.setUserMosquitto(userMosquitto);
		aclMosquitto.setFechaCreacion(new Date());

		return aclMosquitto;
	}

	public AclMosquitto dtoToEntity(ACLMosquittoDTO dto) {
		AclMosquitto aclMosquitto = new AclMosquitto();

		aclMosquitto.setTopic(dto.getTopic());
		aclMosquitto.setSuscriptionPermission(dto.isSuscriptionPermission());
		aclMosquitto.setWritePermission(dto.isWritePermission());
		aclMosquitto.setReadPermission(dto.isReadPermission());
		aclMosquitto.setUserMosquitto(dto.getUserMosquitto());
		aclMosquitto.setSensor(dto.getSensor());
		aclMosquitto.setFechaCreacion(new Date());

		return aclMosquitto;
	}

}
