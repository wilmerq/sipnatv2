package co.edu.ucc.sipnat.dto;

import java.util.Date;

public class SensorDetailDTO extends SensorDTO {

	private String sensorTypeName;
	private String magnitude;
	private String topic;
	private String status;
	private Date dateLastData;
	private String unitMeasure;

	public String getSensorTypeName() {
		return sensorTypeName;
	}

	public void setSensorTypeName(String sensorTypeName) {
		this.sensorTypeName = sensorTypeName;
	}

	public String getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateLastData() {
		return dateLastData;
	}

	public void setDateLastData(Date dateLastData) {
		this.dateLastData = dateLastData;
	}

	public String getUnitMeasure() {
		return unitMeasure;
	}

	public void setUnitMeasure(String unitMeasure) {
		this.unitMeasure = unitMeasure;
	}

}
