package co.edu.ucc.sipnat.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@SuppressWarnings("serial")
public class Dato extends CamposComunesdeEntidad {

	@ManyToOne
	private Sensor sensor;
	private String valor;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date fechaRecoleccion;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date fechaSincronizacion;
	private Boolean revisado;
	private String protocolo;

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Date getFechaRecoleccion() {
		return fechaRecoleccion;
	}

	public void setFechaRecoleccion(Date fechaRecoleccion) {
		this.fechaRecoleccion = fechaRecoleccion;
	}

	public Date getFechaSincronizacion() {
		return fechaSincronizacion;
	}

	public void setFechaSincronizacion(Date fechaSincronizacion) {
		this.fechaSincronizacion = fechaSincronizacion;
	}

	public Boolean getRevisado() {
		return revisado;
	}

	public void setRevisado(Boolean revisado) {
		this.revisado = revisado;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

}
