//package co.edu.ucc.sipnat.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//
//@Entity
//@SuppressWarnings("serial")
//public class CordenadaDeLaZona extends CamposComunesdeEntidad implements Serializable{
//
//	@ManyToOne
//	private ZonaAfectada zonaAfectada;
//	private String latitud;
//	private String longitud;
//	private Integer orden;
//
//	public ZonaAfectada getZonaAfectada() {
//		return zonaAfectada;
//	}
//
//	public void setZonaAfectada(ZonaAfectada zonaAfectada) {
//		this.zonaAfectada = zonaAfectada;
//	}
//
//	public String getLatitud() {
//		return latitud;
//	}
//
//	public void setLatitud(String latitud) {
//		this.latitud = latitud;
//	}
//
//	public String getLongitud() {
//		return longitud;
//	}
//
//	public void setLongitud(String longitud) {
//		this.longitud = longitud;
//	}
//
//	public Integer getOrden() {
//		return orden;
//	}
//
//	public void setOrden(Integer orden) {
//		this.orden = orden;
//	}
//
//}
