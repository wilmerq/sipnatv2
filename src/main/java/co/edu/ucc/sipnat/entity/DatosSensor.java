//package co.edu.ucc.sipnat.entity;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//@Entity
//@SuppressWarnings("serial")
//public class DatosSensor extends CamposComunesdeEntidad implements Serializable {
//
//	@ManyToOne
//	private Sensor sensor;
//	private String dato;
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date fechaRecoleccion;
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date fechaSincronizacion;
//	private String horaDeRecoleccion;
//	private Boolean revisado;
//
//	public Sensor getSensor() {
//		return sensor;
//	}
//
//	public void setSensor(Sensor sensor) {
//		this.sensor = sensor;
//	}
//
//	public String getDato() {
//		return dato;
//	}
//
//	public void setDato(String dato) {
//		this.dato = dato;
//	}
//
//	public Date getFechaRecoleccion() {
//		return fechaRecoleccion;
//	}
//
//	public void setFechaRecoleccion(Date fechaRecoleccion) {
//		this.fechaRecoleccion = fechaRecoleccion;
//	}
//
//	public Date getFechaSincronizacion() {
//		return fechaSincronizacion;
//	}
//
//	public void setFechaSincronizacion(Date fechaSincronizacion) {
//		this.fechaSincronizacion = fechaSincronizacion;
//	}
//
//	public String getHoraDeRecoleccion() {
//		return horaDeRecoleccion;
//	}
//
//	public void setHoraDeRecoleccion(String horaDeRecoleccion) {
//		this.horaDeRecoleccion = horaDeRecoleccion;
//	}
//
//	public Boolean getRevisado() {
//		return revisado;
//	}
//
//	public void setRevisado(Boolean revisado) {
//		this.revisado = revisado;
//	}
//
//}
