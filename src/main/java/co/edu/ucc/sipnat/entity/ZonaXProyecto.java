//package co.edu.ucc.sipnat.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//
//@Entity
//@SuppressWarnings("serial")
//public class ZonaXProyecto extends CamposComunesdeEntidad implements Serializable {
//
//	@ManyToOne
//	private Proyecto proyecto;
//	@ManyToOne
//	private ZonaAfectada zonaAfectada;
//
//	public Proyecto getProyecto() {
//		return proyecto;
//	}
//
//	public void setProyecto(Proyecto proyecto) {
//		this.proyecto = proyecto;
//	}
//
//	public ZonaAfectada getZonaAfectada() {
//		return zonaAfectada;
//	}
//
//	public void setZonaAfectada(ZonaAfectada zonaAfectada) {
//		this.zonaAfectada = zonaAfectada;
//	}
//}
