//package co.edu.ucc.sipnat.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Transient;
//
//@Entity
//@SuppressWarnings("serial")
//public class Proyecto extends CamposComunesdeEntidad implements Serializable {
//
//	private String nombre;
//	@Column(columnDefinition = "TEXT")
//	private String descripcion;
//	private String alertaNivel1;
//	private String alertaNivel2;
//	private String alertaNivel3;
//	private String usuario;
//	@Transient
//	private Boolean selecionado;
//
//	public String getNombre() {
//		return nombre;
//	}
//
//	public void setNombre(String nombre) {
//		this.nombre = nombre;
//	}
//
//	public String getDescripcion() {
//		return descripcion;
//	}
//
//	public void setDescripcion(String descripcion) {
//		this.descripcion = descripcion;
//	}
//
//	public String getAlertaNivel1() {
//		return alertaNivel1;
//	}
//
//	public void setAlertaNivel1(String alertaNivel1) {
//		this.alertaNivel1 = alertaNivel1;
//	}
//
//	public String getAlertaNivel2() {
//		return alertaNivel2;
//	}
//
//	public void setAlertaNivel2(String alertaNivel2) {
//		this.alertaNivel2 = alertaNivel2;
//	}
//
//	public String getAlertaNivel3() {
//		return alertaNivel3;
//	}
//
//	public void setAlertaNivel3(String alertaNivel3) {
//		this.alertaNivel3 = alertaNivel3;
//	}
//
//	public String getUsuario() {
//		return usuario;
//	}
//
//	public void setUsuario(String usuario) {
//		this.usuario = usuario;
//	}
//
//	public Boolean getSelecionado() {
//		return selecionado;
//	}
//
//	public void setSelecionado(Boolean selecionado) {
//		this.selecionado = selecionado;
//	}
//}
