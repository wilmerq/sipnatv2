package co.edu.ucc.sipnat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.DatoNotificacion;

@Repository
public interface IDatoNotificacionDAO extends JpaRepository<DatoNotificacion, Long> {
	
	@Query(value = "select dn from DatoNotificacion dn where dn.notificacion.id=:idNotification order by dn.dato.fechaRecoleccion")
	public List<DatoNotificacion> getAllDataNotification(@Param("idNotification") Long idNotification);

}
