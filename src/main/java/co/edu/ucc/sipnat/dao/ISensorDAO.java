package co.edu.ucc.sipnat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.Sensor;

@Repository
public interface ISensorDAO extends JpaRepository<Sensor, Long>{
	
	

}
