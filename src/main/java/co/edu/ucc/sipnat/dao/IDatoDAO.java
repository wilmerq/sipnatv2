package co.edu.ucc.sipnat.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.Dato;

@Repository
public interface IDatoDAO extends JpaRepository<Dato, Long> {

	@Query(value = "select * from dato where sensor_id=:idSensor order by fecha_creacion DESC limit 1", nativeQuery = true)
	public Dato getFirstDatoByFechaRecoleccion(@Param("idSensor") Long idSensor);

	@Query(value = "select d from Dato d where d.sensor.id=:idSensor and d.fechaRecoleccion >= :date order by d.fechaRecoleccion")
	public List<Dato> getDataSensorByFechaRecoleccion(@Param("idSensor") Long idSensor, @Param("date") Date date);
	
	@Query(value = "select d from Dato d where d.sensor.id=:idSensor order by d.fechaRecoleccion")
	public List<Dato> getAllDataSensor(@Param("idSensor") Long idSensor);
	
	@Query(value = "select d from Dato d where d.sensor.id=:idSensor and d.revisado=false order by d.fechaRecoleccion")
	public List<Dato> getAllDataSensorNotRevised(@Param("idSensor") Long idSensor);
}
